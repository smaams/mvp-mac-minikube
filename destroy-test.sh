#!/usr/bin/env bash

kubectl delete -f test/canary-percentage/ts.yaml || true
kubectl delete -f test/canary-percentage/load.yaml || true
kubectl delete -f test/canary-percentage/hello.yaml || true
