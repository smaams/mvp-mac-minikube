#!/usr/bin/env bash

# check kubectl is installed or not
echo "Checking kubectl ..."
if ! [ -x "$(command -v kubectl)" ]; then
  echo "Error: kubectl is not installed." >&2
  echo "Follow the doc to install kubectl: " >&2
  echo "https://kubernetes.io/docs/tasks/tools/install-kubectl/" >&2
  exit 1
fi

# check current context
echo "Checking current context ..."
echo "You are using context: $(kubectl config current-context)"
read -p "Continue? " -n 1 -r
echo
if ! [[ $REPLY =~ ^[Yy]$ ]]
then
    exit 1
fi

# check connection to k8s by kubectl version
echo "Testing connection to k8s ..."
if kubectl version >/dev/null 2>&1 ; then
  echo "Testing connecting to k8s done."
else
  echo "Error: Missing or incomplete kube config info." >&2
  echo "Please check your kube config file, default location is ~/.kube/config." >&2
  exit 1
fi

# check k8s server minor version
echo "Checking k8s version ..."
if k8s_server_minior_version=$(kubectl version -o json | jq ".serverVersion.minor" | sed 's/[^0-9]*//g'); then
  if (( $k8s_server_minior_version < 13 )); then
    echo "Error: k8s minimum version 1.13 required."
    exit 1
  fi
else
  echo "Error: detecting k8s server version failed."
  exit 1
fi

# check service mesh CLI is installed or not
echo "Checking linkerd ..."
if ! [ -x "$(command -v linkerd)" ]; then
  # install Linkerd CLI
  echo "Linkerd not installed yet, installing Linkerd ..."
  brew install linkerd
else
  echo "Linkerd already installed!"
fi

echo "Checking service mesh version ..."
linkerd version

if linkerd version | grep Server | grep -q '[0-9]'; then
  echo "Info: it seems you already have service mesh installed."
  exit 0
fi

echo "Service mesh pre-install check ..."
if linkerd check --pre ; then
  echo "Service mesh install ..."
  linkerd install | kubectl apply -f -
else
  echo "Error: pre-install check failed, see outout above." >&2
  exit 1
fi

echo "Wait until linkerd is ready ..."
linkerd check

echo "Enabling auto injection in all namespaces ..."
for ns in $(kubectl get namespaces -o json | jq -r .items[].metadata.name); do
  if [[ ! "$ns" =~ ^kube ]] && [[ ! "$ns" =~ ^linkerd ]]; then
    kubectl annotate --overwrite namespace $ns linkerd.io/inject=enabled
  fi
done

# ingress for service mesh dashboard
# should be a customised dashboard
echo "Creating ingress for service mesh dashboard ..."
kubectl apply -f ./ingress.yaml

# automated canary
echo "Installing automated canary release tool ..."
kubectl apply -k github.com/weaveworks/flagger/kustomize/linkerd
