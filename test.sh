#!/usr/bin/env bash

echo "Setting up percentage (random) based canary deployment test ..."
kubectl delete -f test/canary-percentage/ts.yaml || true
kubectl apply -f test/canary-percentage/hello.yaml
kubectl apply -f test/canary-percentage/load.yaml

echo "Wait until pods are ready ..."
kubectl -n default wait --for=condition=available --timeout=30s deployment/hello-v1
kubectl -n default wait --for=condition=available --timeout=30s deployment/hello-v2

echo "Observing traffic in 3 seconds"
sleep 1
echo "Observing traffic in 2 seconds"
sleep 1
echo "Observing traffic in 1 second, press q to continue in observing mode"
sleep 1
linkerd top deploy/load-generator

# apply traffic shift 90/10
echo "Shifting traffic now, 90/10 canary ..."
kubectl apply -f test/canary-percentage/ts.yaml
echo "Observing traffic in 3 seconds"
sleep 1
echo "Observing traffic in 2 seconds"
sleep 1
echo "Observing traffic in 1 second, press q to continue in observing mode"
sleep 1
linkerd top deploy/load-generator

echo "Testing done!"
echo "You can also observe the traffic in UI:"
echo "http://192.168.64.6/namespaces/default/trafficsplits/hello-canary"
